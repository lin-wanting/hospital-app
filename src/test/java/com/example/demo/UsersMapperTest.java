package com.example.demo;

import com.example.demo.mapper.UsersMapper;
import com.example.demo.pojo.Users;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UsersMapperTest {

    @Autowired  //自动注入的意思
    UsersMapper usersMapper;

    @Test   //测试方法没有返回值，没有参数，不能私有
    void selectByPrimaryKeyTest(){
        Users user=usersMapper.selectByPrimaryKey("12345671111");
        System.out.println(user.getRealName());
    }
}
