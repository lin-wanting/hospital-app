package com.example.demo;

import com.example.demo.mapper.SetmealMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.SQLOutput;
import java.util.List;

@SpringBootTest
public class SetmealMapperTest {
    @Autowired
    SetmealMapper setmealMapper;

    @Test
    void test(){
        List<Integer> ids = setmealMapper.selectBySnId(1);
        System.out.println(ids.toString());
    }
}
