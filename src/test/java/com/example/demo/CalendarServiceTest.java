package com.example.demo;

import com.example.demo.dto.CalendarRequestDto;
import com.example.demo.dto.CalendarResponseDto;
import com.example.demo.service.CalendarService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class CalendarServiceTest {

    @Autowired
    CalendarService calendarService;

    @Test
    void testGetCalendar(){
        CalendarRequestDto calendarRequestDto = new CalendarRequestDto();
        calendarRequestDto.setHpId(1);
        calendarRequestDto.setMonth(9);
        calendarRequestDto.setYear(2024);
        List<CalendarResponseDto> calendarResponseDtoList = calendarService.listAppointmentCalendar(calendarRequestDto);
        System.out.println("size:"+calendarResponseDtoList.size());

        for (CalendarResponseDto crd:calendarResponseDtoList){
            System.out.println(crd.getYmd());
            System.out.println(crd.getRemainder()+"-"+crd.getRemainder());
        }
    }
}
