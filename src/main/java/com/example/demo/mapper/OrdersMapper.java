package com.example.demo.mapper;

import com.example.demo.dto.CalendarResponseDto;
import com.example.demo.dto.OrdersMapperDto;
import com.example.demo.dto.OrdersPageRequestDto;
import com.example.demo.pojo.Orders;
import com.example.demo.pojo.Users;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface OrdersMapper {
    int deleteByPrimaryKey(Integer orderId);

    int insert(Orders record);

    int insertSelective(Orders record);

    Orders selectByPrimaryKey(Integer orderId);

    int updateByPrimaryKeySelective(Orders record);

    int updateByPrimaryKey(Orders record);

    List<CalendarResponseDto> listOrdersAppointmentNumber(List<OrdersMapperDto> parameList);

    //根据 smId 查询 订单ID
    @Select("select orderId from orders where smId=#{smId}")
    public Integer selectOrderId(Integer smId);

    @Update("update orders SET state =#{state} where orderId=#{orderId}")
    int updateState(Integer orderId,Integer state);

    void updateById(Orders orders);

    @Select("select price from setmeal where smId=#{smId}")
    public Integer getPrice(Integer smId);

    @Select("select name from setmeal where smId=#{smId}")
    public String getSetmealName(Integer smId);

    @Select("select name from hospital where hpId=#{hpId}")
    public String getHospitalName(Integer hpId);

    @Select("select userId, realName, sex from users where userId=#{userId}")
    public Users getUser(String userId);

    @Select("select orderId, userId, orderDate, hpId, smId, state from orders where userId=#{userId}")
    List<Orders> getUserOrders(String userId);


    //根据条件做预约订单的分页查询
    List<Orders> listOrders(OrdersPageRequestDto request);

    //根据条件查询预约订单总行数
    int getOrdersCount(OrdersPageRequestDto request);

    public Orders getOrdersById(Integer orderId);

    @Update("update orders set state=#{state} where orderId=#{orderId}")
    public int updateOrdersState(Orders orders);


}