package com.example.demo.mapper;

import com.example.demo.pojo.Hospital;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface HospitalMapper {
    int deleteByPrimaryKey(Integer hpId);

    int insert(Hospital record);

    int insertSelective(Hospital record);

    Hospital selectByPrimaryKey(Integer hpId);

    int updateByPrimaryKeySelective(Hospital record);

    int updateByPrimaryKeyWithBLOBs(Hospital record);

    int updateByPrimaryKey(Hospital record);

    // 采用注解开发对数据库进行增删改查
    @Select("select hpId, name, picture, telephone, address, businessHours, deadline, rule, state " +
            "from hospital where state=#{state} order by hpId")
    public List<Hospital> selectList(Integer state);
}