package com.example.demo.mapper;

import com.example.demo.pojo.Setmeal;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SetmealMapper {
    int deleteByPrimaryKey(Integer smId);

    int insert(Setmeal record);

    int insertSelective(Setmeal record);

    Setmeal selectByPrimaryKey(Integer smId);

    int updateByPrimaryKeySelective(Setmeal record);

    int updateByPrimaryKey(Setmeal record);


    @Select("select smId, name, type, price from setmeal where type=#{type} order by smId")
    public List<Setmeal> selectByType(Integer type);

    //根据套餐编号查询检查项目编号
    @Select("select ciId from setmealdetailed where smId=#{smId}")
    public List<Integer> selectBySnId(Integer smId);

    @Select("select smId, name, type, price from setmeal where smId=#{smId}")
    public Setmeal getSetmealById(Integer smId);

    public Setmeal getSetmealById2(Integer smId);
}