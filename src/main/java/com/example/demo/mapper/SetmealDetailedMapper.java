package com.example.demo.mapper;

import com.example.demo.pojo.SetmealDetailed;

import java.util.List;

public interface SetmealDetailedMapper {
//    int deleteByPrimaryKey(Integer sdId);
//
//    int insert(SetmealDetailed record);
//
//    int insertSelective(SetmealDetailed record);
//
//    SetmealDetailed selectByPrimaryKey(Integer sdId);
//
//    int updateByPrimaryKeySelective(SetmealDetailed record);
//
//    int updateByPrimaryKey(SetmealDetailed record);

    public List<SetmealDetailed> listSetmealDetailedBySmId(Integer smId);
}