package com.example.demo.mapper;

import com.example.demo.pojo.CiDetailedReport;
import com.example.demo.pojo.CiReport;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface CiDetailedReportMapper {
    int deleteByPrimaryKey(Integer cidrId);

    int insert(CiDetailedReport record);

    int insertSelective(CiDetailedReport record);

    CiDetailedReport selectByPrimaryKey(Integer cidrId);

    int updateByPrimaryKeySelective(CiDetailedReport record);

    int updateByPrimaryKey(CiDetailedReport record);

    public int saveCiDetailedReport(List<CiDetailedReport> list);

    @Select("select * from ciDetailedReport where orderId=#{orderId} and ciId=#{ciId} order by cidrId")
    public List<CiDetailedReport> listCiDetailedReportByOrderIdByCiId(CiDetailedReport ciDetailedReport);

    public int updateCiDetailedReport(List<CiDetailedReport> list);

    @Select("select * from cireport where orderId=#{orderId}")
    public List<CiReport> selectByOrderId(Integer orderId);

    @Select("select cidrId, name, unit, minrange, maxrange, normalValue, normalValueString, type, value, \n" +
            "    isError, ciId, orderId from cidetailedreport where ciId=#{ciId} AND orderId=#{orderId}")
    public List<CiDetailedReport> getReportDetailByciId(Integer ciId,Integer orderId);

    @Select("select * from cidetailedreport where name=#{name}")
    CiDetailedReport selectByName(String name);

}