package com.example.demo.mapper;

import com.example.demo.pojo.OverallResult;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface OverallResultMapper {
    int deleteByPrimaryKey(Integer orId);

    int insert(OverallResult record);

    int insertSelective(OverallResult record);

    OverallResult selectByPrimaryKey(Integer orId);

    int updateByPrimaryKeySelective(OverallResult record);

    int updateByPrimaryKey(OverallResult record);

    @Select("select * from overallResult where orderId=#{orderId} order by orId")
    public List<OverallResult> listOverallResultByOrderId(Integer orderId);
    @Insert("insert into overallResult values(null,#{title},#{content},#{orderId})")
    public int saveOverallResult(OverallResult overallResult);
    @Update("update overallResult set title=#{title},content=#{content} where orId=#{orId}")
    public int updateOverallResult(OverallResult overallResult);
    @Delete("delete from overallResult where orId=#{orId}")
    public int removeOverallResult(Integer orId);

    @Select("select orId, title, content, orderId from OverallResult where orderId=#{orderId}")
    public List<OverallResult> selectByOrderId(Integer orderId);
}