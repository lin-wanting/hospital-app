package com.example.demo.mapper;

import com.example.demo.pojo.CiReport;
import com.example.demo.pojo.OverallResult;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Service;

import java.util.List;

public interface CiReportMapper {
    int deleteByPrimaryKey(Integer cirId);

    int insert(CiReport record);

    int insertSelective(CiReport record);

    CiReport selectByPrimaryKey(Integer cirId);

    int updateByPrimaryKeySelective(CiReport record);

    int updateByPrimaryKey(CiReport record);


//    cirId, ciId, ciName, orderId
    @Select("select count(*) from cireport where orderId =#{orderId}")
    int getCiReportByOrderId(Integer orderId);

    public int saveCiReport(List<CiReport> list);

    @Select("select * from ciReport where orderId=#{orderId}")
    public List<CiReport> listCiReport(Integer orderId);

    @Select("select cirId, ciId, ciName, orderId from CiReport where orderId=#{orderId}")
    public List<CiReport> selectByOrderId(Integer orderId);

}