package com.example.demo.mapper;

import com.example.demo.pojo.Doctor;
import org.apache.ibatis.annotations.Select;

public interface DoctorMapper {
    int deleteByPrimaryKey(Integer docId);

    int insert(Doctor record);

    int insertSelective(Doctor record);

    Doctor selectByPrimaryKey(Integer docId);

    int updateByPrimaryKeySelective(Doctor record);

    int updateByPrimaryKey(Doctor record);

    @Select("select docId, docCode, realName, password, sex from doctor where docCode=#{docCode}")
    Doctor selectBydocCode(String docCode);
}