package com.example.demo.dto;
// 获取日历时服务端会响应会前端的数据结构
public class CalendarResponseDto {

    private String ymd;         //预约日期   2024-9-20
    private Integer total;      //最大预约人数   200
    private Integer existing;   //现有预约人数  2024年9月20日当天订单表有多少人预约了这家医院  13
    private Integer remainder;  //剩余预约人数  187

    public CalendarResponseDto() {}

    public CalendarResponseDto(String ymd) {
        this.ymd = ymd;
    }

    public String getYmd() {
        return ymd;
    }
    public void setYmd(String ymd) {
        this.ymd = ymd;
    }
    public Integer getTotal() {
        return total;
    }
    public void setTotal(Integer total) {
        this.total = total;
    }
    public Integer getExisting() {
        return existing;
    }
    public void setExisting(Integer existing) {
        this.existing = existing;
    }
    public Integer getRemainder() {
        return remainder;
    }
    public void setRemainder(Integer remainder) {
        this.remainder = remainder;
    }

    @Override
    public String toString() {
        return "CalendarResponseDto{" +
                "ymd='" + ymd + '\'' +
                ", total=" + total +
                ", existing=" + existing +
                ", remainder=" + remainder +
                '}';
    }
}
