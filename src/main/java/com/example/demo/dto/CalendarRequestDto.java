package com.example.demo.dto;

public class CalendarRequestDto {

    //前端传了三个参数过来：year month hpId
    private Integer hpId;
    private Integer year;
    private Integer month;

    public Integer getHpId() {
        return hpId;
    }

    public void setHpId(Integer hpId) {
        this.hpId = hpId;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }




}
