package com.example.demo.response;

public class ApiRestResponse<T> {
    private Integer status;
    private String desc;
    //定义泛型，需要在类上声明泛型
    private T data;

    //对应成功这个特殊情况的变量
    private final static Integer SUCCESS_OK=200;
    private final static String SUCCESS_DESC="处理成功";


    //构造方法  成功方法 失败方法  包含数据的，不包含数据的构造方法

    //因为controller方法中都要返回该类型，所以必须提供丰富的构造方法或者实例化方法
    //定制一个构造方法，含状态码和描述信息的初始化
    public ApiRestResponse(Integer status,String desc)
    {
        this.status=status;
        this.desc=desc;
    }

    //定制一个构造方法，含状态码和描述信息，数据data，初始化
    public ApiRestResponse(Integer status,String desc,T data)
    {
        this.status=status;
        this.desc=desc;
        this.data=data;
    }

    //提供一些静态方法，能够快速将我们当前这个类实例化
    //获取对应成功的api响应实例
    public static <T> ApiRestResponse<T> success()
    {
        return new ApiRestResponse<>(SUCCESS_OK,SUCCESS_DESC);
    }

    public static <T> ApiRestResponse<T> success(T data)
    {
        return new ApiRestResponse<>(SUCCESS_OK,SUCCESS_DESC,data);
    }

    //获取对应失败的api响应实例
    public static <T> ApiRestResponse<T> error(tijianExceptionEnum e)
    {
        return new ApiRestResponse<>(e.getStatus(),e.getDesc());
    }

    public static <T> ApiRestResponse<T> error(Integer status,String desc,T data)
    {
        //ApiRestResponse<T> response=new ApiRestResponse<>(status, desc,data);

        return new ApiRestResponse<>(status, desc,data);
    }

    public static <T> ApiRestResponse<T> error(Integer status,String desc)
    {
        return new ApiRestResponse<>(status, desc);
    }



    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
