package com.example.demo.response;

public class TijianException extends RuntimeException{
    private Integer status;
    private String desc;

    public TijianException(Integer status,String desc){
        this.status = status;
        this.desc = desc;
    }

    public TijianException(){

    }

    public TijianException(tijianExceptionEnum exceptionEnum){
        this.status = exceptionEnum.getStatus();
        this.desc = exceptionEnum.getDesc();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
