package com.example.demo.response;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Object handlerException(Exception e){
//        log.error("exception:"+e);
        return ApiRestResponse.error(tijianExceptionEnum.SYSTEM_ERROR);
    }

    @ExceptionHandler(TijianException.class) //声明只处理商城业务异常
    @ResponseBody  //Object--json
    public Object TijianException(TijianException e)
    {
        //1.对于异常要重视，要日志输出
//        log.error("exception:"+e);
        //2.返回一个标准响应格式给用户
        return ApiRestResponse.error(e.getStatus(),e.getDesc());
    }


}
