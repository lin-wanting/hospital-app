package com.example.demo.response;

public enum tijianExceptionEnum {
    NO_TIJIAN_TAOCAN(1001,"无此套餐"),
    NO_HOSPITAL(1002,"此订单已取消"),
    ORDERS_CANCEL(1003,"该日期非工作日"),
            IS_NOT_GONGZUORI(1004,"该日期非工作日"),
    PHONE_OR_PASSWORD_IS_WRONG(1005,"手机号或密码错误"),
    SYSTEM_ERROR(1006,"系统异常"),
    USER_IS_AREADY_EXIST(1007,"该用户已存在"),
    WUFAGENGGAI_NEWPASSWORD(1008,"信息错误无法更改密码"),

    PASSWORD_UPDATE_FAILED(1009,"密码修改失败"),
    ORDERS_EXIST(1010,"此订单已存在"),
    DOCTOR_NO_EXIST(1011,"医生不存在"),
    ORDER_NO_CANCEL1(1012,"预约日期已过，不可取消预约"),
    ORDER_NO_CANCEL2(1013,"预约日期距离当前日期不足两天，不可取消预约");

    //1001:无此套餐      1002：无此医院   1003：此订单已取消         1004：该日期非工作日

    private Integer status;
    private String desc;

    tijianExceptionEnum(Integer status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
