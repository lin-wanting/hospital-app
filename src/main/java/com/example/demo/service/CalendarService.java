package com.example.demo.service;

import com.example.demo.dto.CalendarRequestDto;
import com.example.demo.dto.CalendarResponseDto;

import java.util.List;

public interface CalendarService {
    static void main(String[] args) {
        List<CalendarResponseDto> crdList = new CalendarServiceImpl().getCurrentCalendarList(2024, 2);
        System.out.println(crdList.toString());
    }

    //生成预约日历
    List<CalendarResponseDto> listAppointmentCalendar(CalendarRequestDto dto);


    //获取当前年和当前月的日历   //
    List<CalendarResponseDto> getCurrentCalendarList(Integer year, Integer month);
}
