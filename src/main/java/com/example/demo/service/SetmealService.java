package com.example.demo.service;

import com.example.demo.pojo.Setmeal;

import java.util.List;

public interface SetmealService {
    //根据类型查询套餐列表数据的方法
    List<Setmeal> listByType(Setmeal setmeal);

    Setmeal getSetmealById(Integer smId);

    Setmeal getSetmealBySmId(Integer smId);
}
