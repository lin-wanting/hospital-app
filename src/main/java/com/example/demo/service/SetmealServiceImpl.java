package com.example.demo.service;

import com.example.demo.mapper.CheckItemMapper;
import com.example.demo.mapper.SetmealMapper;
import com.example.demo.pojo.CheckItem;
import com.example.demo.pojo.Setmeal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SetmealServiceImpl implements SetmealService {

    @Autowired
    SetmealMapper setmealMapper;

    @Autowired
    CheckItemMapper checkItemMapper;

    //根据类型查询套餐列表数据的方法
    @Override
    public List<Setmeal> listByType(Setmeal setmeal){
//        System.out.println("into listByType1...........................");
        List<Setmeal> setmealList = setmealMapper.selectByType(setmeal.getType()); //第一步

//        System.out.println("into listByType2...........................");
        //第二步
        for (Setmeal sm:setmealList){

//            System.out.println("into listByType3...........................");
            Integer smId = sm.getSmId(); //获取套餐编号
//            System.out.println(smId);

            //获取对应检查项编号集合
            List<Integer> ciIdList = setmealMapper.selectBySnId(smId);
//            System.out.println(ciIdList);

            //对检查项编号集合进行遍历，拿着每个编号去检查项表中查询检查项所有信息
            List<CheckItem> checkItemList = new ArrayList<>();
            for (Integer ciId:ciIdList){
                System.out.println(1);
                CheckItem checkItem = checkItemMapper.selectByPrimaryKey(ciId);
                //每查询一个出来，就给到套餐对象
                checkItemList.add(checkItem);
//                System.out.println("checkItem.CiName:"+checkItem.getCiName());
//                System.out.println("checkItem.Meaning:"+checkItem.getMeaning());
//                System.out.println("checkItemList:"+checkItemList);
            }
            sm.setCheckItemList(checkItemList);
//            System.out.println("sm::"+sm);
        }

        System.out.println("into listByType5....."+setmealList);
        return setmealList; //这个返回既包含套餐数据，又包含检查项数据
    }

    @Override
    public Setmeal getSetmealById(Integer smId){
//        System.out.println("套餐查询之前。。。。。。。。。。。。");
        Setmeal sm = setmealMapper.selectByPrimaryKey(smId);  //selectByPrimaryKey
        System.out.println("sm："+sm);
//        System.out.println("套餐查询之后。。。。。。。。。。。。");
        System.out.println("套餐名字"+sm.getName());
        return sm;
    }


    @Override
    public Setmeal getSetmealBySmId(Integer smId){
        System.out.println("套餐查询之前。。。。。。。。。。。。");
        Setmeal sm = setmealMapper.selectByPrimaryKey(smId);  //selectByPrimaryKey
        System.out.println("sm："+sm);
        System.out.println("套餐查询之后。。。。。。。。。。。。");
        System.out.println("套餐名字"+sm.getName());
        return sm;
    }
}
