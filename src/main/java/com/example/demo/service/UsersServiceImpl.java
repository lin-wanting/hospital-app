package com.example.demo.service;

import com.example.demo.mapper.UsersMapper;
import com.example.demo.pojo.Users;
import com.example.demo.response.ApiRestResponse;
import com.example.demo.response.TijianException;
import com.example.demo.response.tijianExceptionEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

@Service
public class UsersServiceImpl implements UsersService {

    @Autowired
    UsersMapper usersMapper;


    //处理登录业务
    //调用mapper中的方法查询该用户是否存在， 存在返回返回该用户数据， 不存在报错

    @Override
    public Users login(Users users){
        Users user=usersMapper.selectByPrimaryKey(users.getUserId());
        System.out.println(user);

        //无加密：
//        if ((user!=null) && (users.getPassword().equals(user.getPassword()))){
//            return user;
//        }else {
//            throw new TijianException(tijianExceptionEnum.PHONE_OR_PASSWORD_IS_WRONG);
//        }
        System.out.println("登录盐值:"+user.getSalt());
        if (user != null) {
            // 使用用户输入的密码和数据库中的盐值生成哈希值
            String inputPasswordMd5 = encryptMD5(users.getPassword(), user.getSalt());
            System.out.println(inputPasswordMd5);
            // 比较哈希值
            if (inputPasswordMd5.equals(user.getPassword())) {
                System.out.println("两者相等。。。。。。。。。。。。。");
                return user;
            } else {
                System.out.println("两者不相等111111111111。。。。。。。。。。。。。");
                throw new TijianException(tijianExceptionEnum.PHONE_OR_PASSWORD_IS_WRONG);
            }
        } else {
            System.out.println("两者不相等2222222222222。。。。。。。。。。。。。");
            throw new TijianException(tijianExceptionEnum.PHONE_OR_PASSWORD_IS_WRONG);
        }

    }


    @Override
    public ApiRestResponse register(Users users){

        // 生成随机盐值
        SecureRandom random = new SecureRandom();
        String salt = new BigInteger(130, random).toString(32);

        System.out.println("注册盐值:"+salt);
        // 对密码进行加密
        String passwordMd5 = encryptMD5(users.getPassword(), salt);

        // 将加密后的密码和盐值保存到用户对象中
        users.setPassword(passwordMd5);
        users.setSalt(salt);
//        users.setSalt("salt");

        //先查询再插入数据
        Users u = usersMapper.selectByPrimaryKey(users.getUserId());
        if (u == null){
            System.out.println("没找到了。。。。。。。。。");
            //没有查到 说明没有注册过 即没有主键冲突
            users.setUserType(1);//默认是普通用户
            usersMapper.insert(users); //默认是自动提交
        }else {
            System.out.println("找到了..........");
            throw new TijianException(tijianExceptionEnum.USER_IS_AREADY_EXIST);
        }

       return ApiRestResponse.success();
    }


    // MD5加密方法
    public static String encryptMD5(String password, String salt) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update((salt + password).getBytes());
            byte[] bytes = md.digest();
            StringBuilder sb = new StringBuilder();
            for (byte aByte : bytes) {
                sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new RuntimeException("MD5 algorithm not available.", e);
        }
    }


    @Override
    public ApiRestResponse newpassword(Users users){

        //先查询再插入数据
        Users u = usersMapper.selectByPrimaryKey(users.getUserId());
        if ((u!=null) && (users.getRealName().equals(u.getRealName())) && (users.getIdentityCard().equals(u.getIdentityCard()))){
            System.out.println("找到了。。。。。。。。。");
            //没有查到 说明没有注册过 即没有主键冲突
            // 更新用户密码  MD5加密
            String newSalt = new BigInteger(130, new SecureRandom()).toString(32);
            String newPasswordMd5 = encryptMD5(users.getPassword(), newSalt);
            u.setPassword(newPasswordMd5);
            u.setSalt(newSalt);
            usersMapper.updateByPrimaryKeySelective(u); //默认是自动提交
        }else {
            System.out.println("没找到了..........");
            throw new TijianException(tijianExceptionEnum.WUFAGENGGAI_NEWPASSWORD);
        }

        return ApiRestResponse.success();
    }

    @Override
    public Users getUserByUserId(String userId){
        Users users = usersMapper.selectByPrimaryKey(userId);
        System.out.println("我获取到的Users："+users);
        return users;
    }
}
