package com.example.demo.service;

import com.example.demo.dto.OrdersPageRequestDto;
import com.example.demo.dto.OrdersPageResponseDto;
import com.example.demo.pojo.Orders;
import com.example.demo.response.ApiRestResponse;

import java.util.List;

public interface OrdersService {
    Orders saveOrders(Orders orders);

    ApiRestResponse InsertOrders(Orders orders);

    List<Orders> getOrder(Orders orders);

    OrdersPageResponseDto listOrders(OrdersPageRequestDto request);

    Orders getOrdersById(Integer orderId);

    ApiRestResponse<Object> cancelOrder(Integer orderId);

    int updateOrdersState(Orders orders);
    List<Orders> getReport(Orders orders);
}
