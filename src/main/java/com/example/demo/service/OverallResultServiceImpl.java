package com.example.demo.service;

import com.example.demo.mapper.CiDetailedReportMapper;
import com.example.demo.mapper.CiReportMapper;
import com.example.demo.mapper.OverallResultMapper;
import com.example.demo.pojo.CiDetailedReport;
import com.example.demo.pojo.CiReport;
import com.example.demo.pojo.OverallResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OverallResultServiceImpl implements OverallResultService{

    @Autowired
    private OverallResultMapper overallResultMapper;
    @Autowired
    private CiReportMapper ciReportMapper;
    @Autowired
    private CiDetailedReportMapper ciDetailedReportMapper;

    @Override
    public List<OverallResult> listOverallResultByOrderId(Integer orderId) {
        System.out.println("这是总结论："+overallResultMapper.listOverallResultByOrderId(orderId));
        return overallResultMapper.listOverallResultByOrderId(orderId);
    }

    @Override
    public int removeOverallResult(Integer orId) {
        return overallResultMapper.removeOverallResult(orId);
    }

    @Override
    public int saveOverallResult(OverallResult overallResult) {
        return overallResultMapper.saveOverallResult(overallResult);
    }

    @Override
    public int updateOverallResult(OverallResult overallResult) {
        return overallResultMapper.updateOverallResult(overallResult);
    }

    @Override
    public List<OverallResult> getOverallResultById(OverallResult overallResult){

        System.out.println("into....1.......");
        List<OverallResult> OverallResultList =overallResultMapper.selectByOrderId(overallResult.getOrderId());
        System.out.println("into....2.......");
        return OverallResultList;
    }

    @Override
    public List<CiReport> getReportByOrderId(CiReport ciReport) {
        System.out.println("into....3.......");
        List<CiReport> ReportList =ciReportMapper.selectByOrderId(ciReport.getOrderId());
        System.out.println("into....4.......");
        return ReportList;
    }

    @Override
    public List<CiReport> getReportDetailByCiId(CiReport ciReport) {
        System.out.println("into....5.......");

        // 根据 orderId 获取报告列表
        List<CiReport> reportDetailList = ciDetailedReportMapper.selectByOrderId(ciReport.getOrderId());
        System.out.println("into....6.......");

        // 创建一个新的列表来存储合并后的结果
        List<CiReport> combinedReportList = new ArrayList<>();
        Integer orderId=ciReport.getOrderId();
        // 遍历每个报告
        for (CiReport report : reportDetailList) {
            Integer ciId = report.getCiId();


            // 根据 ciId 获取详细信息
            List<CiDetailedReport> detail = ciDetailedReportMapper.getReportDetailByciId(ciId,orderId);

            // 将去重后的详细信息添加到报告中
            report.setDetail(detail);

            // 将合并后的报告添加到结果列表中
            combinedReportList.add(report);
        }

        System.out.println("into....7.......");
        return combinedReportList;
    }

}
