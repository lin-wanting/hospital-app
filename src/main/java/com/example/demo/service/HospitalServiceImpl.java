package com.example.demo.service;

import com.example.demo.mapper.HospitalMapper;
import com.example.demo.pojo.Hospital;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

//实现类
@Service
public class HospitalServiceImpl implements HospitalService {
    @Autowired
    HospitalMapper hospitalMapper;

    @Override
    public List<Hospital> list(Hospital hospital){
        //处理查询医院列表数据的方法，正常营业的医院
        List<Hospital> list = hospitalMapper.selectList(hospital.getState());
        return list;
    }

    @Override
    public Hospital getHospitalById(Integer hpId){
        Hospital h = hospitalMapper.selectByPrimaryKey(hpId);
        System.out.println("医院："+h);
        //name, telephone, address, businessHours, deadline, rule, state
        System.out.println(h.getName());
        return h;
    }

}
