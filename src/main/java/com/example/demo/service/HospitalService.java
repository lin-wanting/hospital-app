package com.example.demo.service;

import com.example.demo.pojo.Hospital;
import org.springframework.stereotype.Service;

import java.util.List;

public interface HospitalService {

    List<Hospital> list(Hospital hospital);

    Hospital getHospitalById(Integer HpId);
}
