package com.example.demo.service;

import com.example.demo.pojo.Users;
import com.example.demo.response.ApiRestResponse;

public interface UsersService {
    Users login(Users users);

//    Users register(Users users);

    ApiRestResponse register(Users users);

    ApiRestResponse newpassword(Users users);

    Users getUserByUserId(String userId);
}
