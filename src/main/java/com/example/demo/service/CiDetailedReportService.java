package com.example.demo.service;

import com.example.demo.pojo.CiDetailedReport;

import java.util.List;

public interface CiDetailedReportService {
    int updateCiDetailedReport(List<CiDetailedReport> list);
}
