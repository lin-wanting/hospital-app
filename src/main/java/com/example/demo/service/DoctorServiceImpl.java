package com.example.demo.service;

import com.example.demo.mapper.DoctorMapper;
import com.example.demo.pojo.Doctor;
import com.example.demo.response.TijianException;
import com.example.demo.response.tijianExceptionEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DoctorServiceImpl implements DoctorService{

    @Autowired
    DoctorMapper doctorMapper;

    @Override
    public Doctor login(Doctor doctor){
        Doctor doctor1 = doctorMapper.selectBydocCode(doctor.getDocCode());

        if(doctor1!=null){
            return doctor1;
        }else {
            //不存在抛出异常
            throw new TijianException(tijianExceptionEnum.USER_IS_AREADY_EXIST);
        }
    }



//    @Autowired
//    DoctorMapper doctorMapper;
//
//    @Override
//    public Doctor doctorLogin(Doctor doctor) {
//        System.out.println(doctor.getDocCode());
//        Doctor dor = doctorMapper.selectByDocCode(doctor.getDocCode());
//        System.out.println(dor);
//        if (dor != null && dor.getPassword().equals(doctor.getPassword())){
//            return dor;
//        }else {
//            throw new TijianException(tijianExceptionEnum.PHONE_OR_PASSWORD_IS_WRONG);
//        }
//    }
}
