package com.example.demo.service;

import com.example.demo.pojo.CiReport;
import com.example.demo.pojo.Orders;

import java.util.List;

public interface CiReportService {
    Integer createReportTemplate(Orders orders);

    List<CiReport> listCiReport(Integer orderId);
}
