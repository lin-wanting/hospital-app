package com.example.demo.service;

import com.example.demo.pojo.CiReport;
import com.example.demo.pojo.OverallResult;

import java.util.List;

public interface OverallResultService {
    List<OverallResult> listOverallResultByOrderId(Integer orderId);

    int removeOverallResult(Integer orId);

    int saveOverallResult(OverallResult overallResult);

    int updateOverallResult(OverallResult overallResult);

    List<OverallResult> getOverallResultById(OverallResult overallResult);

    List<CiReport> getReportByOrderId(CiReport ciReport);

    List<CiReport> getReportDetailByCiId(CiReport ciReport);
}
