package com.example.demo.service;

import com.example.demo.mapper.CiDetailedReportMapper;
import com.example.demo.pojo.CiDetailedReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CiDetailedReportServiceImpl implements CiDetailedReportService{

    @Autowired
    private CiDetailedReportMapper ciDetailedReportMapper;

//    updateCiDetailedReport
    @Override
    public int updateCiDetailedReport(List<CiDetailedReport> list) {
        return ciDetailedReportMapper.updateCiDetailedReport(list);
    }
}
