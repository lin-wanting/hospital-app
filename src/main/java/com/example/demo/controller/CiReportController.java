package com.example.demo.controller;

import com.example.demo.pojo.CiReport;
import com.example.demo.pojo.Orders;
import com.example.demo.service.CiReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/ciReport")
public class CiReportController {

    @Autowired
    CiReportService ciReportService;

    @RequestMapping("/createReportTemplate")
    public Integer createReportTemplate(@RequestBody Orders orders){
        System.out.println();
        Integer integer=ciReportService.createReportTemplate(orders);
        System.out.println("是否成功生成表："+integer);
        return ciReportService.createReportTemplate(orders);
    }


    @RequestMapping("/listCiReport")
    public List<CiReport> listCiReport(@RequestBody CiReport ciReport) {
    return ciReportService.listCiReport(ciReport.getOrderId());
}

}
