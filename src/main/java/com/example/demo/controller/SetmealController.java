package com.example.demo.controller;

import com.example.demo.pojo.Setmeal;
import com.example.demo.response.ApiRestResponse;
import com.example.demo.service.SetmealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/setmeal")
public class SetmealController {

    @Autowired
    SetmealService setmealService;

    @RequestMapping("/listByType")
    public ApiRestResponse listByType(@RequestBody Setmeal setmeal){
        return ApiRestResponse.success(setmealService.listByType(setmeal));
    }

    @RequestMapping("/getSetmealById")
    public ApiRestResponse getSetmealById(@RequestBody Setmeal setmeal){
//        System.out.println("套餐："+setmealService.getSetmealById(setmeal.getSmId()));
        return ApiRestResponse.success(setmealService.getSetmealById(setmeal.getSmId()));
    }

//    getSetmealBySmId  根据SmIdId查找套餐信息
//    @RequestMapping("/getSetmealBySmId")
//    public Setmeal getSetmealBySmId(@RequestBody Setmeal setmeal){
//        System.out.println("套餐："+setmealService.getSetmealById(setmeal.getSmId()));
//    return setmealService.getSetmealById(setmeal.getSmId());
//}


}
