package com.example.demo.controller;

import com.example.demo.mapper.HospitalMapper;
import com.example.demo.pojo.Hospital;
import com.example.demo.pojo.Setmeal;
import com.example.demo.pojo.Users;
import com.example.demo.response.ApiRestResponse;
import com.example.demo.service.HospitalService;
import com.example.demo.service.UsersService;
import com.example.demo.service.UsersServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

//@Controller  //表示它是一个控制器
// @CrossOrigin  //跨域 谁都能访问 但不安全
@RestController  // @Controller +  @RequestBody:返回结果是一个JSON字符串
@RequestMapping("/users")
public class UsersController {

    @Autowired
    UsersService usersService;

    //处理登录请求
    //RequestBody 表示把前端传过来的JSON字符串转化成java对象，给到Users
    @RequestMapping("/login")  //所以url请求： http://localhost:8080/users/login  记住还要JSON字符串
    public Map<String,Objects> login(@RequestBody Users users){

        Map map = new HashMap<>();
        map.put("result","ok");

        return map;
    }
    @RequestMapping("/login2")  //所以url请求： http://localhost:8080/users/login2  记住还要JSON字符串
    public Users login2(@RequestBody Users users){


        return usersService.login(users);  //这个返回结果有 两种情况 一种有数据 一种是null
    }
    @RequestMapping("/login3")  //所以url请求： http://localhost:8080/users/login3  记住还要JSON字符串
    public ApiRestResponse<Users> login3(@RequestBody Users users){

        Users user=usersService.login(users);

        return ApiRestResponse.success(user);  //这个返回结果有 两种情况 一种有数据 一种是null
    }
    //注册
    @RequestMapping("/register")  //所以url请求： http://localhost:8080/users/register  记住还要JSON字符串
    public ApiRestResponse register(@RequestBody Users users){

        return usersService.register(users);
    }

    //忘记密码
    //注册
    @RequestMapping("/newpassword")  //所以url请求： http://localhost:8080/users/newpassword  记住还要JSON字符串
    public ApiRestResponse newpassword(@RequestBody Users users){

        return usersService.newpassword(users);
    }

    @RequestMapping("/getusersById")
    public Users getusersById(@RequestBody Users users){
        System.out.println("USERID：：：：：：：：：：："+users.getUserId());
        return usersService.getUserByUserId(users.getUserId());
    }

}
