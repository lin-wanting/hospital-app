package com.example.demo.controller;

import com.example.demo.dto.OrdersPageRequestDto;
import com.example.demo.pojo.Hospital;
import com.example.demo.pojo.Orders;
import com.example.demo.response.ApiRestResponse;
import com.example.demo.service.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/orders")
public class OrdersController {

    @Autowired
    OrdersService ordersService;

    @RequestMapping("/saveOrders")
    public ApiRestResponse saveOrders(@RequestBody Orders orders){

//        return ApiRestResponse.success(hospitalService.list(hospital));
        return ApiRestResponse.success(ordersService.saveOrders(orders));
    }

    @RequestMapping("/cancelOrder")
    public ApiRestResponse<?> cancelOrder(@RequestBody Map<String, Object> params){
        Integer orderId = (Integer) params.get("orderId");
        System.out.println(orderId);
        return ApiRestResponse.success(ordersService.cancelOrder(orderId));
    }

    @RequestMapping("/createorder")  //所以url请求：  http://localhost:8080/users/register 记住还要参数json字符串
    public ApiRestResponse createorder(@RequestBody Orders orders){

        return ordersService.InsertOrders(orders);

    }
    @RequestMapping("/getOrderList")  //所以url请求：  http://localhost:8080/users/register 记住还要参数json字符串
    public ApiRestResponse getOrderList(@RequestBody Orders orders){

        return ApiRestResponse.success(ordersService.getOrder(orders));
    }

    @RequestMapping("/listOrders")
    public ApiRestResponse listOrders(@RequestBody OrdersPageRequestDto request) {
        return ApiRestResponse.success(ordersService.listOrders(request));
    }

    @RequestMapping("/getOrdersById")
    public Orders getOrdersById(@RequestBody Orders orders) {
        return ordersService.getOrdersById(orders.getOrderId());
    }

    @RequestMapping("/updateOrdersState")
    public int updateOrdersState(@RequestBody Orders orders) {
        return ordersService.updateOrdersState(orders);
    }

    @RequestMapping("/getReportList")  //所以url请求：  http://localhost:8080/users/register 记住还要参数json字符串
    public ApiRestResponse getReportList(@RequestBody Orders orders){

        return ApiRestResponse.success(ordersService.getReport(orders));

    }

}
