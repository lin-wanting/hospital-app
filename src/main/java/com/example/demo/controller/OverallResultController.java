package com.example.demo.controller;

import com.example.demo.pojo.CiReport;
import com.example.demo.pojo.OverallResult;
import com.example.demo.response.ApiRestResponse;
import com.example.demo.service.OverallResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/overallResult")
public class OverallResultController {

    @Autowired
    private OverallResultService overallResultService;

    @RequestMapping("/listOverallResultByOrderId")
    public List<OverallResult> listOverallResultByOrderId(@RequestBody OverallResult overallResult) {
        return overallResultService.listOverallResultByOrderId(overallResult.getOrderId());
    }

    @RequestMapping("/saveOverallResult")
    public int saveOverallResult(@RequestBody OverallResult overallResult) {
        return overallResultService.saveOverallResult(overallResult);
    }

    @RequestMapping("/updateOverallResult")
    public int updateOverallResult(@RequestBody OverallResult overallResult) {
        return overallResultService.updateOverallResult(overallResult);
    }

    @RequestMapping("/removeOverallResult")
    public int removeOverallResult(@RequestBody OverallResult overallResult) {
        return overallResultService.removeOverallResult(overallResult.getOrId());
    }

    @RequestMapping("/getOverallResultById")
    public ApiRestResponse getOverallResultById(@RequestBody OverallResult overallResult){
        System.out.println("into.2..........");

        return ApiRestResponse.success(overallResultService.getOverallResultById(overallResult));
    }

    @RequestMapping("/getReportByOrderId")
    public ApiRestResponse getReportByOrderId(@RequestBody CiReport ciReport){
        return ApiRestResponse.success(overallResultService.getReportByOrderId(ciReport));
    }

    @RequestMapping("/getNameByCiId")
    public ApiRestResponse getNameByCiId(@RequestBody CiReport ciReport){
        System.out.println("运行中");
        return ApiRestResponse.success(overallResultService.getReportDetailByCiId(ciReport));
    }

}
