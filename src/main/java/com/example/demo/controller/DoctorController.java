package com.example.demo.controller;

import com.example.demo.pojo.Doctor;
import com.example.demo.response.ApiRestResponse;
import com.example.demo.service.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/doctor")
public class DoctorController {

    @Autowired
    DoctorService doctorService;

    @RequestMapping("/login")
    public ApiRestResponse<Doctor> login(@RequestBody Doctor doctor){
        Doctor doctor1 = doctorService.login(doctor);

        return ApiRestResponse.success(doctor1);
    }


}
