package com.example.demo.controller;

import com.example.demo.dto.CalendarRequestDto;
import com.example.demo.response.ApiRestResponse;
import com.example.demo.service.CalendarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalendarController {

    @Autowired
    CalendarService calendarService;


    @RequestMapping("/getCalendar")
    public ApiRestResponse getClendar(@RequestBody CalendarRequestDto calendarRequestDto){ // 前端传了三个参数过来： year， month， hpId

        return ApiRestResponse.success(calendarService.listAppointmentCalendar(calendarRequestDto));
    }
}
