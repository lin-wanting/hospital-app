package com.example.demo.controller;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.example.demo.config.AliPayConfig;
import com.example.demo.mapper.OrdersMapper;
import com.example.demo.mapper.SetmealMapper;
import com.example.demo.pojo.Orders;
import com.example.demo.pojo.Setmeal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/pay")
public class PayController {

    @Autowired
    private OrdersMapper ordersMapper;
    @Autowired
    private SetmealMapper setmealMapper;

    @Autowired
    private AlipayClient alipayClient;
    @Autowired
    private AliPayConfig aliPayConfig;

    @RequestMapping("/alipay")
    public String alipay(@RequestBody Orders order ) throws
            IOException, AlipayApiException {

        // 实例化客户端,填入所需参数
        AlipayTradePagePayRequest request = new
                AlipayTradePagePayRequest();

        // 在公共参数中设置回跳和通知地址
        request.setReturnUrl(aliPayConfig.getRETURN_URL());
        request.setNotifyUrl(aliPayConfig.getNOTIFY_URL());


        Integer orderId = order.getOrderId();
        System.out.println("这是我传过来的订单日期："+order.getOrderDate());
        System.out.println("这是我传过来的订单Id："+orderId);

        // 商户订单号，商户网站订单系统中唯一订单号，必填
//        String out_trade_no = String.valueOf(orderId);

        String out_trade_no = orderId + "";

        Integer smId = order.getSmId();
        System.out.println("smId"+smId);
        Setmeal setmeal = setmealMapper.selectByPrimaryKey(smId);
        Integer sPrice = setmeal.getPrice();

        // 付款金额，必填

        String total_amount = sPrice + "";

        // 订单名称，必填
        String subject = setmeal.getName() + "";

        // 商品描述，可空
        String body = "";
//        String body = order.getSetmeal().getName();

        request.setBizContent("{\"out_trade_no\":\"" + out_trade_no + "\","
                + "\"total_amount\":\"" + total_amount + "\","
                + "\"subject\":\"" + subject + "\","
                + "\"body\":\"" + body + "\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

        String form = "";

        form = alipayClient.pageExecute(request).getBody(); // 调用SDK生成表单

        System.out.println("这是我的表单："+form);

//        order.setState(1);

        return form;
    }

    @RequestMapping(value = "/returnUrl")
//    public void returnUrl(HttpServletRequest request, HttpServletResponse
//            response)
//            throws IOException, AlipayApiException {
//        // 获取支付宝GET过来反馈信息
//        Map<String, String> params = new HashMap<String, String>();
//        Map<String, String[]> requestParams = request.getParameterMap();
//        for (String name : requestParams.keySet()) {
//            String[] values = (String[]) requestParams.get(name);
//            String valueStr = "";
//            for (int i = 0; i < values.length; i++) {
//                valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr +
//                        values[i] + ",";
//            }
//            // 解决乱码
//            valueStr = new String(valueStr.getBytes(StandardCharsets.UTF_8),
//                    StandardCharsets.UTF_8);
//            params.put(name, valueStr);
//        }
//
//        System.out.println(params);// 查看参数都有哪些
//        boolean signVerified = AlipaySignature.rsaCheckV1(params,
//                aliPayConfig.getALIPAY_PUBLIC_KEY(), aliPayConfig.getCHARSET(),
//                aliPayConfig.getSIGN_TYPE()); // 调用SDK验证签名
//        // 验证签名通过
//        if(signVerified){
//            // 商户订单号
//            String out_trade_no = new
//                    String(request.getParameter("out_trade_no").getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
//
//            // 支付宝交易号
//            String trade_no = new
//                    String(request.getParameter("trade_no").getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
//
//            // 付款金额
//            String total_amount = new
//                    String(request.getParameter("total_amount").getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
//
//            System.out.println("商户订单号 = " + out_trade_no);
//            System.out.println("支付宝交易号 = " + trade_no);
//            System.out.println("付款金额 = " + total_amount);
//
//            // 支付成功，修改支付状态
//            Orders orders = new Orders();
//            orders.setOrderId(Integer.valueOf(out_trade_no));
//            orders.setState(2);
//            ordersMapper.updateByPrimaryKeySelective(orders);
//            response.sendRedirect("http://localhost:5173/appointmentSuccess");
//        }else{
//            response.sendRedirect("http://localhost:5173/appointmentSuccess");  // http://localhost:5173/appointmentFail
//        }
//    }
//}



    public void returnUrl(HttpServletRequest request, HttpServletResponse
            response)
            throws IOException, AlipayApiException {
        // 获取支付宝GET过来反馈信息
        Map<String, String> params = new HashMap<String, String>();
        Map<String, String[]> requestParams = request.getParameterMap();
        for (String name : requestParams.keySet()) {
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr +
                        values[i] + ",";
            }
            // 解决乱码
            valueStr = new String(valueStr.getBytes(StandardCharsets.UTF_8),
                    StandardCharsets.UTF_8);
            params.put(name, valueStr);
        }

        System.out.println(params);// 查看参数都有哪些
        boolean signVerified = AlipaySignature.rsaCheckV1(params,
                aliPayConfig.getALIPAY_PUBLIC_KEY(), aliPayConfig.getCHARSET(),
                aliPayConfig.getSIGN_TYPE()); // 调用SDK验证签名
        // 验证签名通过
        if(signVerified){
            // 商户订单号
            String out_trade_no = new
                    String(request.getParameter("out_trade_no").getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);

            // 支付宝交易号
            String trade_no = new
                    String(request.getParameter("trade_no").getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);

            // 付款金额
            String total_amount = new
                    String(request.getParameter("total_amount").getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);

            System.out.println("商户订单号 = " + out_trade_no);
            System.out.println("支付宝交易号 = " + trade_no);
            System.out.println("付款金额 = " + total_amount);

            // 支付成功，修改支付状态
//            Orders orders = new Orders();
//            orders.setOrderId(Integer.valueOf(out_trade_no));
//            orders.setState(2);
//            ordersMapper.updateByPrimaryKey(orders);

            Orders orders = new Orders();
            orders.setOrderId(Integer.valueOf(out_trade_no));
            Integer i = ordersMapper.updateState(orders.getOrderId(),2);
            System.out.println("----------------------------");
            System.out.println(i);
            response.sendRedirect("http://localhost:5173/appointmentSuccess");
        }else{
            response.sendRedirect("http://localhost:5173/appointmentCancel");
        }
    }
}



//    public void returnUrl(HttpServletRequest request, HttpServletResponse
//            response)
//            throws IOException, AlipayApiException {
//        // 获取支付宝GET过来反馈信息
//        Map<String, String> params = new HashMap<String, String>();
//        Map<String, String[]> requestParams = request.getParameterMap();
//        for (String name : requestParams.keySet()) {
//            String[] values = (String[]) requestParams.get(name);
//            String valueStr = "";
//            for (int i = 0; i < values.length; i++) {
//                valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr +
//                        values[i] + ",";
//            }
//// 解决乱码
//            valueStr = new String(valueStr.getBytes(StandardCharsets.UTF_8),
//                    StandardCharsets.UTF_8);
//            params.put(name, valueStr);
//        }
//
//        System.out.println(params);// 查看参数都有哪些
//        boolean signVerified = AlipaySignature.rsaCheckV1(params,
//                aliPayConfig.getALIPAY_PUBLIC_KEY(), aliPayConfig.getCHARSET(),
//                aliPayConfig.getSIGN_TYPE()); // 调用SDK验证签名
//// 验证签名通过
//        if (signVerified) {
//// 商户订单号
//            String out_trade_no = new
//                    String(request.getParameter("out_trade_no").getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
//
//// 支付宝交易号
//            String trade_no = new
//                    String(request.getParameter("trade_no").getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
//
//// 付款金额
//            String total_amount = new
//                    String(request.getParameter("total_amount").getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
//
//            System.out.println("商户订单号 = " + out_trade_no);
//            System.out.println("支付宝交易号 = " + trade_no);
//            System.out.println("付款金额 = " + total_amount);
//
//// 支付成功，修改支付状态
//            Orders orders = new Orders();
//            orders.setOrderId(Integer.valueOf(out_trade_no));
////            orders.setState(2);
//            Integer i = ordersMapper.updateState(orders.getOrderId(), 2);
//            System.out.println("-----------");
//            System.out.println(i);
//            response.sendRedirect("http://localhost:5173/appointmentsuccess");
//        } else {
//            response.sendRedirect("http://localhost:5173/appointmentcancel");
//        }
//    }}