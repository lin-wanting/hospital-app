package com.example.demo.controller;

import com.example.demo.pojo.Hospital;
import com.example.demo.response.ApiRestResponse;
import com.example.demo.service.HospitalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hospital")
public class HospitalController {

    @Autowired
    HospitalService hospitalService;

    @RequestMapping("/list")
    public ApiRestResponse list(@RequestBody Hospital hospital){

        return ApiRestResponse.success(hospitalService.list(hospital));
    }

    @RequestMapping("/getHospitalById")
    public ApiRestResponse getHospitalById(@RequestBody Hospital hospital){

//        hospitalService.getHospitalById(hospital);
//        System.out.println("医院："+hospitalService.getHospitalById(hospital.getHpId()));
        return ApiRestResponse.success(hospitalService.getHospitalById(hospital.getHpId()));
    }
}
